<?php

namespace spec\Domain\SharedKernel;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

use Domain\SharedKernel\Money\Currency;
use Domain\SharedKernel\Money;
use Domain\SharedKernel\Money\Exception\InvalidMoneyAmountException;
use Domain\SharedKernel\Money\Exception\DifferentCurrenciesException;

class MoneySpec extends ObjectBehavior
{
    function it_throws_exception_if_created_with_begative_amount()
    {
        $this->beConstructedWith(-1, new Currency('PLN'));

        $this->shouldThrow(InvalidMoneyAmountException::class)
            ->duringInstantiation();
    }

    function it_has_amount()
    {
        $this->beConstructedWith(1000, new Currency('PLN'));

        $this->getAmount()->shouldReturn(1000);
    }

    function it_has_currency()
    {
        $currency = new Currency('PLN');
        $this->beConstructedWith(1000, $currency);

        $this->getCurrency()->shouldReturn($currency);
    }

    function it_can_be_compared_to_other_money()
    {
        $currency = new Currency('PLN');
        $this->beConstructedWith(1000, $currency);

        $equal = new Money(1000, $currency);

        $this->isEqualTo($equal)->shouldReturn(true);

        $differentAmount = new Money(1, $currency);

        $this->isEqualTo($differentAmount)->shouldReturn(false);
    }

    function it_adds_other_money()
    {
        $currency = new Currency('PLN');
        $this->beConstructedWith(1000, $currency);

        $other = new Money(500, $currency);

        $sum = $this->add($other);

        $sum->shouldBeAnInstanceOf(Money::class);
        $sum->getAmount()->shouldBe(1500);

        $this->getAmount()->shouldBe(1000);
    }

    function it_throws_exception_when_trying_to_add_in_different_currencies()
    {
        $this->beConstructedWith(1000, new Currency('PLN'));

        $other = new Money(500, new Currency('EUR'));

        $this->shouldThrow(DifferentCurrenciesException::class)
            ->during('add', [$other]);
    }
}
