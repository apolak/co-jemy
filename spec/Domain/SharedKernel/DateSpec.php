<?php

namespace spec\Domain\SharedKernel;

use Domain\SharedKernel\Exception\InvalidDateFormatException;
use PhpSpec\ObjectBehavior;

class DateSpec extends ObjectBehavior
{
    function it_throws_exception_when_invalid_date_format_is_provided()
    {
        $date = 'invalid_date_format';

        $this->beConstructedWith($date);

        $this->shouldThrow(InvalidDateFormatException::class)->duringInstantiation();
    }
}
