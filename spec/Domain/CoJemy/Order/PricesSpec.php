<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Type;
use Domain\SharedKernel\Money;
use PhpSpec\ObjectBehavior;

class PricesSpec extends ObjectBehavior
{
    function it_returns_prices_with_delivery_cost()
    {
        $prices = $this->addDeliveryCost(Money::fromString('100', 'PLN'));
        $prices->shouldBeAnInstanceOf(Prices::class);
        $prices->getDeliveryCost()->shouldBeLike(new Price(Type::deliveryCost(), Money::fromString('100', 'PLN')));
    }

    function it_returns_prices_with_price_per_package()
    {
        $prices = $this->addPricePerPackage(Money::fromString('100', 'PLN'));
        $prices->shouldBeAnInstanceOf(Prices::class);
        $prices->getPricePerPackage()->shouldBeLike(new Price(Type::pricePerPackage(), Money::fromString('100', 'PLN')));
    }

    function it_returns_prices_with_given_total_amount()
    {
        $prices = $this->addTotalAmount(Money::fromString('100', 'PLN'));

        $prices->shouldBeAnInstanceOf(Prices::class);
        $prices->getTotalAmount()->shouldBeLike(new Price(Type::total(), Money::fromString('100', 'PLN')));
    }
}
