<?php

namespace spec\Domain\CoJemy\Order\Events;

use Domain\CoJemy\Order\ParametersBag;
use PhpSpec\ObjectBehavior;
use Domain\CoJemy\Event;

class OrderClosedEventSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('id123');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('OrderClosedEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'id123');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
