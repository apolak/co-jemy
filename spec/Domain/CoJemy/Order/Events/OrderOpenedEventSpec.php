<?php

namespace spec\Domain\CoJemy\Order\Events;

use Domain\CoJemy\Order\ParametersBag;
use Domain\CoJemy\Order\HashHolder;
use PhpSpec\ObjectBehavior;
use Domain\CoJemy\Event;

class OrderOpenedEventSpec extends ObjectBehavior
{
    function let()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123');
        
        $this->beConstructedWith('id123', 'shama', $hashHolder->toArray(), 100, 200, 'PLN', 'admin123');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('OrderOpenedEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'id123');
        $expectedParametersBag->setParameter('supplierId', 'shama');
        $expectedParametersBag->setParameter('pricePerPackage', 100);
        $expectedParametersBag->setParameter('deliveryCost', 200);
        $expectedParametersBag->setParameter('hashes', ['adminHash' => 'adminHash123']);
        $expectedParametersBag->setParameter('adminId', 'admin123');
        $expectedParametersBag->setParameter('currency', 'PLN');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
