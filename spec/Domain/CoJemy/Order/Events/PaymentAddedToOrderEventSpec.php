<?php

namespace spec\Domain\CoJemy\Order\Events;

use PhpSpec\ObjectBehavior;
use Domain\CoJemy\Order\ParametersBag;
use Domain\CoJemy\Event;

class PaymentAddedToOrderEventSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('order123', 'payment-123', 'Jon doe', 1000, 'PLN', 'position-123');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('PaymentAddedToOrderEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'order123');
        $expectedParametersBag->setParameter('paymentId', 'payment-123');
        $expectedParametersBag->setParameter('payerName', 'Jon doe');
        $expectedParametersBag->setParameter('paymentAmount', 1000);
        $expectedParametersBag->setParameter('paymentCurrency', 'PLN');
        $expectedParametersBag->setParameter('positionId', 'position-123');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
