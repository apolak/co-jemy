<?php

namespace spec\Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;
use PhpSpec\ObjectBehavior;

class PositionWasRemovedFromOrderEventSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('order123', 'user123', 'pos123');
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('PositionWasRemovedFromOrderEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'order123');
        $expectedParametersBag->setParameter('userId', 'user123');
        $expectedParametersBag->setParameter('positionId', 'pos123');

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
