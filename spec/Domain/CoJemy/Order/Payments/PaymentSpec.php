<?php

namespace spec\Domain\CoJemy\Order\Payments;

use Domain\CoJemy\Order\Payments\Payment;
use Domain\CoJemy\Order\Payments\PaymentId;
use Domain\CoJemy\Order\Positions\NullPositionId;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\SharedKernel\Money;
use PhpSpec\ObjectBehavior;

class PaymentSpec extends ObjectBehavior
{
    function it_creates_payment_with_position_id()
    {
        $this->beConstructedThrough('withPositionId', ['some-id', 'payer-name', 1000, 'PLN', 'position-id']);

        $this->getPositionId()->shouldBeLike(UniqIdPositionId::fromString('position-id'));
        $this->getId()->shouldBeLike(PaymentId::fromString('some-id'));
        $this->getPayerName()->shouldBe('payer-name');
        $this->getPaymentValue()->shouldBeLike(Money::fromString(10, 'PLN'));
    }

    function it_creates_payment_without_position_id()
    {
        $this->beConstructedThrough('withoutPositionId', ['some-id', 'payer-name', 1000, 'PLN']);

        $this->getPositionId()->shouldBeLike(NullPositionId::generate());
        $this->getId()->shouldBeLike(PaymentId::fromString('some-id'));
        $this->getPayerName()->shouldBe('payer-name');
        $this->getPaymentValue()->shouldBeLike(Money::fromString(10, 'PLN'));
    }

    function it_returns_true_if_payments_are_equal()
    {
        $this->beConstructedThrough('withPositionId', ['some-id', 'payer-name', 1000, 'PLN', 'position-id']);
        $samePayment = Payment::withPositionId('some-id', 'payer-name', 1000, 'PLN', 'position-id');

        $this->isEqualTo($samePayment)->shouldReturn(true);
    }

    function it_returns_false_if_payments_are_not_equal()
    {
        $this->beConstructedThrough('withPositionId', ['some-id', 'payer-name', 1000, 'PLN', 'position-id']);
        $otherPayment = Payment::withPositionId('other-some-id', 'payer-name', 1000, 'PLN', 'position-id');

        $this->isEqualTo($otherPayment)->shouldReturn(false);
    }
}
