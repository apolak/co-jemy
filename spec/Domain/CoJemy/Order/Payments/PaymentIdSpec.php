<?php

namespace spec\Domain\CoJemy\Order\Payments;

use PhpSpec\ObjectBehavior;

class PaymentIdSpec extends ObjectBehavior
{
    function it_generates_random_payment_id()
    {
        $this->beConstructedThrough('generate');
        $this->__toString()->shouldBeString();
    }

    function it_creates_payment_id_from_string()
    {
        $this->beConstructedThrough('fromString', ['some-id']);
        $this->__toString()->shouldBe('some-id');
    }
}
