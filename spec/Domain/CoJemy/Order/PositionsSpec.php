<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Exception\Order\AccessDeniedException;
use Domain\CoJemy\Order\Positions\Position;
use Domain\CoJemy\Order\Positions;
use Domain\CoJemy\Order\UserId;
use PhpSpec\ObjectBehavior;

class PositionsSpec extends ObjectBehavior
{
    function it_checks_if_position_exists()
    {
        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', 'position2');
        $newPositions = $newPositions->add($position2);

        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldBeLike(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position3'))->shouldBeLike(false);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position4'))->shouldBeLike(false);

    }

    function it_allows_to_add_position()
    {
        $position = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1');
        $newPositions = $this->add($position);
        $newPositions->shouldBeAnInstanceOf(Positions::class);
        $newPositions->count()->shouldReturn(1);

        $newPositions = $this->add($position);
        $position = new Position('user2', 'dish2', 'hamburger', 19.00, 'PLN', 'amanda', 'position2');
        $newPositions = $newPositions->add($position);

        $newPositions->shouldBeAnInstanceOf(Positions::class);
        $newPositions->count()->shouldReturn(2);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('not_existent_position'))->shouldReturn(false);
    }

    function it_allows_to_remove_position()
    {
        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', 'position2');
        $newPositions = $newPositions->add($position2);
        $position3 = new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', 'position3');
        $newPositions = $newPositions->add($position3);

        $newPositions->count()->shouldReturn(3);

        $newPositions = $newPositions->remove(Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user1'));
        $newPositions->count()->shouldReturn(2);

        $newPositions->exists(Positions\UniqIdPositionId::fromString('position1'))->shouldReturn(false);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position2'))->shouldReturn(true);
        $newPositions->exists(Positions\UniqIdPositionId::fromString('position3'))->shouldReturn(true);

    }

    function it_does_not_allow_to_remove_position_owned_by_other_user()
    {
        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1'));
        $newPositions = $newPositions->add(new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', 'position2'));
        $newPositions = $newPositions->add(new Position('user2', 'dish1', 'pizza', 23.00, 'PLN', 'john', 'position3'));

        $newPositions->count()->shouldReturn(3);

        $newPositions->shouldThrow(AccessDeniedException::class)->during('remove', [Positions\UniqIdPositionId::fromString('position3'), UserId::fromString('user1')]);
    }

    function it_allows_to_check_if_user_can_remove_position()
    {
        $newPositions = $this->add(new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1'));

        $newPositions->count()->shouldReturn(1);

        $newPositions->shouldNotThrow(AccessDeniedException::class)->during('checkRemovePossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user1')]);
        $newPositions->shouldThrow(AccessDeniedException::class)->during('checkRemovePossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user2')]);
        $newPositions->shouldThrow(AccessDeniedException::class)->during('checkRemovePossibility', [Positions\UniqIdPositionId::fromString('position1'), UserId::fromString('user3')]);
    }

    function it_returns_positions_as_array()
    {
        $position1 = new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1');
        $newPositions = $this->add($position1);
        $position2 = new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', 'position2');
        $newPositions = $newPositions->add($position2);

        $newPositions->count()->shouldReturn(2);

        $newPositions->toArray()->shouldBeArray();
        $newPositions->toArray()->shouldHaveCount(2);
    }

    function it_creates_positions_with_items()
    {
        $positions = [
            new Position('user1', 'dish1', 'pizza', 23.00, 'PLN', 'andy', 'position1'),
            new Position('user1', 'dish2', 'hamburger', 19.00, 'PLN', 'andy', 'position2')
        ];

        $this->beConstructedThrough('createWithItems',[$positions]);

        $this->shouldBeAnInstanceOf(Positions::class);

        $this->count()->shouldReturn(2);

        $this->toArray()->shouldContain($positions[0]);
        $this->toArray()->shouldContain($positions[1]);
    }
}
