Feature: Open order

  Background:
    Given the database is empty
    And there are no orders in the system

  Scenario: Open order with success
    Given there are food suppliers with parameters:
      | id | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      |  1 | supplier-1 | 1000         | 5000                   | 500              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
      |  2 | supplier-2 | 2000         | 6000                   | 700              | 123-456-111 | http://supplier.gov | http://some-menu-url.co |
    When I send a POST request to "/api/orders" with body:
    """
    {
      "supplierId":1
    }
    """
    Then the response code should be 201
    And there should be "1" order with status "opened"
    And order should be placed for supplier "1"
    And order should have delivery cost set to 1000 and price per package to 500
    And the response should contain json:
    """
    {
      "adminHash": "@string@",
      "isAdmin": true,
      "userId": "@string@",
      "id": "@string@",
      "supplier":
      {
        "id":1,
        "name":"supplier-1",
        "deliveryCost":1000,
        "freeDeliveryThreshold":5000,
        "singlePackageCost":500,
        "phoneNumber":"123-456-789",
        "websiteUrl":"http:\/\/supplier.com",
        "menuUrl":"http:\/\/some-menu-url.pl",
        "menuItems":[]
      }
    }
    """

  Scenario: Open order with user id
    Given there are food suppliers with parameters:
      | id | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      |  1 | supplier-1 | 1000         | 5000                   | 500              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
      |  2 | supplier-2 | 2000         | 6000                   | 700              | 123-456-111 | http://supplier.gov | http://some-menu-url.co |
    When I send a POST request to "/api/orders" with body and headers:
    """
    {
      "body":
      {
        "supplierId":1
      },
      "headers":
      {
        "userId": "user123"
      }
    }
    """
    Then the response code should be 201
    And there should be "1" order with status "opened"
    And order should be placed for supplier "1"
    And order should have admin with id "user123"
    And order should have delivery cost set to 1000 and price per package to 500
    And the response should contain json:
    """
    {
      "adminHash": "@string@",
      "isAdmin": true,
      "userId": "user123",
      "id": "@string@",
      "supplier":
      {
        "id":1,
        "name":"supplier-1",
        "deliveryCost":1000,
        "freeDeliveryThreshold":5000,
        "singlePackageCost":500,
        "phoneNumber":"123-456-789",
        "websiteUrl":"http:\/\/supplier.com",
        "menuUrl":"http:\/\/some-menu-url.pl",
        "menuItems":[]
      }
    }
    """

  Scenario: Open order when supplier is not found
    Given there are food suppliers with parameters:
      | id | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      |  1 | supplier-1 | 1000         | 5000                  | 500               | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
      |  2 | supplier-2 | 2000         | 6000                  | 700               | 123-456-111 | http://supplier.gov | http://some-menu-url.co |
    When I send a POST request to "/api/orders" with body:
    """
    {
      "supplierId":3
    }
    """
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"Could not find supplier with id 3."
    }
    """
