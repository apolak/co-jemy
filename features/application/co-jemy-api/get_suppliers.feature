Feature: Get suppliers

  Background:
    Given the database is empty

  Scenario: Get all suppliers
    Given there are food suppliers with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 1000         | 5000                  | 500               | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
      | supplier-2 | 2000         | 6000                  | 700               | 123-456-111 | http://supplier.gov | http://some-menu-url.co |
    When I send a GET request to "/api/suppliers"
    Then the response code should be 200
    And the response should contain json:
    """
    [
      {
        "id":1,
        "name":"supplier-1",
        "deliveryCost":1000,
        "freeDeliveryThreshold":5000,
        "singlePackageCost":500,
        "phoneNumber":"123-456-789",
        "websiteUrl":"http:\/\/supplier.com",
        "menuUrl":"http:\/\/some-menu-url.pl",
        "menuItems":[]
      },
      {
        "id":2,
        "name":"supplier-2",
        "deliveryCost":2000,
        "freeDeliveryThreshold":6000,
        "singlePackageCost":700,
        "phoneNumber":"123-456-111",
        "websiteUrl":"http:\/\/supplier.gov",
        "menuUrl":"http:\/\/some-menu-url.co",
        "menuItems":[]
      }
    ]
    """

  Scenario: Get supplier by id
    Given there are food suppliers with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 1000         | 5000                  | 500              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    And food supplier with id "1" has menu items with parameters:
      | name       | price        |
      | sushi      | 25           |
      | burger     | 12           |
    When I send a GET request to "/api/suppliers/1"
    Then the response code should be 200
    And the response should contain json:
    """
    {
      "id":1,
      "name":"supplier-1",
      "deliveryCost":1000,
      "freeDeliveryThreshold":5000,
      "singlePackageCost":500,
      "phoneNumber":"123-456-789",
      "websiteUrl":"http:\/\/supplier.com",
      "menuUrl":"http:\/\/some-menu-url.pl",
      "menuItems":
      [
        {
          "id":1,
          "name":"sushi",
          "price":25
        },
        {
          "id":2,
          "name":"burger",
          "price":12
        }
      ]
    }
    """

  Scenario: Get 404 when there are no suppliers
    When I send a GET request to "/api/suppliers"
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"Suppliers not found."
    }
    """

  Scenario: Get 404 when there is no supplier with specified id
    When I send a GET request to "/api/suppliers/1"
    Then the response code should be 404
    And the response should contain json:
    """
    {
      "code":404,
      "message":"Supplier not found."
    }
    """
