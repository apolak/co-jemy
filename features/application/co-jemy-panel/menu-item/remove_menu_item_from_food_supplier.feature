@menu-item
Feature: Remove menu item from supplier
  In order to manage menu items
  As an administrator
  I have to be able to remove them

  Scenario: Remove menu item
    Given the database is empty
    And there is food supplier with menu item
    When I am on "/food-suppliers/1/menu-items/1/delete"
    Then I should be on food supplier page
    And there should be no menu items
