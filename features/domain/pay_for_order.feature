@domain @payment
Feature: Pay for order
  As a user I want to...
  So that I...

  Scenario:
    Given there are no orders in the system
    And there is order in the system with id "toPay"
    And there is order in the system with id "toPay" with positions:
      | dishName    | dishPrice | dishId | userId | userNick | positionId |
      | burger      | 1900     | dish1  | user1  | jack     | position1  |
    When I add payment for position with id "position1" with payer "Jon Doe" and value "1000" for order with id "toPay"
    Then the order "toPay" should contain "1" payment
    And the payment should be "Jon Doe", "1000", "position1"
