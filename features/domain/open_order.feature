@domain
Feature: Open new order
  As a user I want to...
  So that I...

  Scenario:
    Given there are no orders in the system
    And there is only a supplier with id "1" and delivery cost "1000" and price per package "200"
    When I open new order for supplier with id "1" and aggregate id "orderId" and admin id "adminId"
    Then there should be "1" order with status "opened"
    And order should be placed for supplier "1"
    And order should have id "orderId"
    And order should have admin with id "adminId"
    And order should have delivery cost set to 1000 and price per package to 200
