<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;
use Infrastructure\CoJemy\Order\Commands\RemovePositionFromOrderCommand;

class RemovePositionFromOrderHandler
{
    private $eventStoreRepository;
    private $eventStorePersister;

    /**
     * RemovePositionFromOrderHandler constructor.
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * Command handler
     * @param RemovePositionFromOrderCommand $command
     */
    public function handleRemovePositionFromOrderCommand(RemovePositionFromOrderCommand $command)
    {
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($command->getOrderId()));

        $order->removePosition(
            $command->getPositionId(),
            $command->getUserId()
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
