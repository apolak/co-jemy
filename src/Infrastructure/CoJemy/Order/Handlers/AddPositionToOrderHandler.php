<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;
use Domain\CoJemy\Order\UserId;
use Domain\SharedKernel\Money;
use Domain\SharedKernel\Money\Currency;
use Infrastructure\CoJemy\Order\Commands\AddPositionToOrderCommand;

class AddPositionToOrderHandler
{
    /**
     * @var EventStoreRepository
     */
    private $eventStoreRepository;

    /**
     * @var EventStorePersister
     */
    private $eventStorePersister;

    /**
     * @param EventStoreRepository $eventStoreRepository
     * @param EventStorePersister $eventStorePersister
     */
    public function __construct(EventStoreRepository $eventStoreRepository, EventStorePersister $eventStorePersister)
    {
        $this->eventStoreRepository = $eventStoreRepository;
        $this->eventStorePersister = $eventStorePersister;
    }

    /**
     * @param AddPositionToOrderCommand $command
     */
    public function handleAddPositionToOrderCommand(AddPositionToOrderCommand $command)
    {
        $order = $this->eventStoreRepository->findOrderById(AggregateId::fromString($command->getOrderId()));
        $userId = $command->getUserId() ? UserId::fromString($command->getUserId()) : UserId::generate();

        $order->addPosition(
            $command->getDishId(),
            $command->getDishName(),
            new Money($command->getPrice(), new Currency($command->getCurrency())),
            $userId->__toString(),
            $command->getUserNick()
        );

        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
