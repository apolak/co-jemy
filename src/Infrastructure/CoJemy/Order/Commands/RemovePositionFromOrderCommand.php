<?php

namespace Infrastructure\CoJemy\Order\Commands;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order\Positions\PositionId;
use Domain\CoJemy\Order\UserId;

class RemovePositionFromOrderCommand
{
    /** @var AggregateId */
    private $orderId;

    /** @var PositionId */
    private $positionId;

    /** @var UserId */
    private $userId;

    public function __construct(AggregateId $orderId, PositionId $positionId, UserId $userId)
    {
        $this->orderId = $orderId;
        $this->positionId = $positionId;
        $this->userId = $userId;
    }

    /**
     * @return AggregateId
     */
    public function getOrderId() : AggregateId
    {
        return $this->orderId;
    }

    /**
     * @return PositionId
     */
    public function getPositionId() : PositionId
    {
        return $this->positionId;
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

}
