<?php

namespace Infrastructure\CoJemy\Order\Commands;

class AddPaymentToOrderCommand
{
    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $positionId;

    /**
     * @var string
     */
    private $payerName;

    /**
     * @var int
     */
    private $paymentAmount;

    /**
     * @var string
     */
    private $paymentCurrency;

    /**
     * @param string $orderId
     * @param string $positionId
     * @param string $payerName
     * @param string $paymentAmount
     * @param string $paymentCurrency
     */
    public function __construct(
        string $orderId,
        string $positionId,
        string $payerName,
        string $paymentAmount,
        string $paymentCurrency
    ) {
        $this->orderId = $orderId;
        $this->positionId = $positionId;
        $this->payerName = $payerName;
        $this->paymentAmount = $paymentAmount;
        $this->paymentCurrency = $paymentCurrency;
    }

    /**
     * @return string
     */
    public function getOrderId() : string
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * @return string
     */
    public function getPayerName() : string
    {
        return $this->payerName;
    }

    /**
     * @return string
     */
    public function getPaymentAmount() : string
    {
        return $this->paymentAmount;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency() : string
    {
        return $this->paymentCurrency;
    }
}
