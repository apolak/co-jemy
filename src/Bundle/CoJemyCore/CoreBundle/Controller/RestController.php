<?php

namespace Bundle\CoJemyCore\CoreBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Infrastructure\CoJemy\Order\Exception\OrderNotFoundException;
use Infrastructure\CoJemy\Order\Exception\SupplierNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;

use Domain\CoJemy\Order\UserId;
use Domain\CoJemy\Aggregate\AggregateId;

class RestController extends FOSRestController
{
    public function getSuppliersAction()
    {
        $suppliers = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->findAll();

        if (count($suppliers) === 0) {
            throw new NotFoundHttpException('Suppliers not found.');
        }

        return $suppliers;
    }

    public function getSupplierAction($id)
    {
        $supplier = $this->getDoctrine()->getRepository('CoreBundle:FoodSupplier')->find($id);

        if (is_null($supplier)) {
            throw new NotFoundHttpException('Supplier not found.');
        }

        return $supplier;
    }

    /**
     * @Post("/orders")
     *
     * @param Request $request
     * @param UserId $userId
     *
     * @return View
     */
    public function openOrderAction(Request $request, UserId $userId)
    {
        $orderId = AggregateId::generate();

        try {
            $this->get('tactician.commandbus')->handle(new OpenOrderCommand(
                $request->get('supplierId'),
                (string)$orderId,
                $userId
            ));
        } catch (SupplierNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        try {
            $orderView = $this->get('co_jemy.view_factory')->createOrderView($orderId, $userId);
        } catch (OrderNotFoundException $e) {
            throw new NotFoundHttpException(
                $e->getMessage(),
                $e
            );
        } catch (SupplierNotFoundException $e) {
            throw new NotFoundHttpException(
                $e->getMessage(),
                $e
            );
        }

        $view = $this->view(
            array_merge(
                ['userId'=>(string) $userId],
                $orderView->toArray()
            ),
            201
        );

        return $view;
    }
}
