<?php

namespace Application;

interface View
{
    /**
     * @return array
     */
    function toArray(): array;
}
