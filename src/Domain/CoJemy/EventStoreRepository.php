<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;

interface EventStoreRepository
{
    /**
     * @param AggregateId $id
     * @return Order|null
     */
    public function findOrderById(AggregateId $id);
}
