<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Total;
use Domain\CoJemy\Order\Prices\Type;
use Domain\SharedKernel\Money;

class Prices
{
    /**
     * @var Price
     */
    private $pricePerPackage;

    /**
     * @var Price
     */
    private $deliveryCost;

    /**
     * @var Price
     */
    private $totalAmount;

    /**
     * @param Money $pricePerPackage
     * @return Prices
     */
    public function addPricePerPackage(Money $pricePerPackage) : Prices
    {
        $modifiedPrices = clone $this;
        $modifiedPrices->pricePerPackage = new Price(Type::pricePerPackage(), $pricePerPackage);

        return $modifiedPrices;
    }

    /**
     * @param Money $deliveryCost
     * @return Prices
     */
    public function addDeliveryCost(Money $deliveryCost) : Prices
    {
        $modifiedPrices = clone $this;
        $modifiedPrices->deliveryCost = new Price(Type::deliveryCost(), $deliveryCost);

        return $modifiedPrices;
    }

    /**
     * @param Money $totalAmount
     * @return Prices
     */
    public function addTotalAmount(Money $totalAmount) : Prices
    {
        $modifiedPrices = clone $this;
        $modifiedPrices->totalAmount = new Price(Type::total(), $totalAmount);

        return $modifiedPrices;
    }

    /**
     * @return Price
     */
    public function getPricePerPackage() : Price
    {
        return $this->pricePerPackage;
    }

    /**
     * @return Price
     */
    public function getDeliveryCost() : Price
    {
        return $this->deliveryCost;
    }

    /**
     * @return Price
     */
    public function getTotalAmount() : Price
    {
        return $this->totalAmount;
    }
}
