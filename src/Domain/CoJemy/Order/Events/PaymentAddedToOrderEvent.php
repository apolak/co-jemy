<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class PaymentAddedToOrderEvent implements Event
{
    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var string
     */
    private $payerName;

    /**
     * @var int
     */
    private $paymentAmount;

    /**
     * @var string
     */
    private $paymentCurrency;

    /**
     * @var string
     */
    private $positionId;

    /**
     * @param string $aggregateId
     * @param string $paymentId
     * @param string $payerName
     * @param int $paymentAmount
     * @param string $paymentCurrency
     * @param string $positionId
     */
    public function __construct(
        string $aggregateId,
        string $paymentId,
        string $payerName,
        int $paymentAmount,
        string $paymentCurrency,
        string $positionId
    ) {
        $this->aggregateId = $aggregateId;
        $this->paymentId = $paymentId;
        $this->payerName = $payerName;
        $this->paymentCurrency = $paymentCurrency;
        $this->paymentAmount = $paymentAmount;
        $this->positionId = $positionId;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'PaymentAddedToOrderEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('paymentId', $this->paymentId);
        $parameters->setParameter('payerName', $this->payerName);
        $parameters->setParameter('paymentAmount', $this->paymentAmount);
        $parameters->setParameter('paymentCurrency', $this->paymentCurrency);
        $parameters->setParameter('positionId', $this->positionId);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return PaymentAddedToOrderEvent
     */
    public static function fromParameters(array $parameters) : PaymentAddedToOrderEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['paymentId'],
            $parameters['payerName'],
            $parameters['paymentAmount'],
            $parameters['paymentCurrency'],
            $parameters['positionId']
        );
    }
}
