<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class OrderClosedEvent implements Event
{
    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @param string $aggregateId
     */
    public function __construct(string $aggregateId)
    {
        $this->aggregateId = $aggregateId;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'OrderClosedEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return OrderClosedEvent
     */
    public static function fromParameters(array $parameters) : OrderClosedEvent
    {
        return new self(
            $parameters['aggregateId']
        );
    }
}
