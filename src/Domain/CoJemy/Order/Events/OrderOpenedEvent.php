<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class OrderOpenedEvent implements Event
{
    /**
     * @var string
     */
    private $aggregateId;

    /**
     * @var string
     */
    private $supplierId;

    /**
     * @var array
     */
    private $hashes;

    /**
     * @var int
     */
    private $pricePerPackage;

    /**
     * @var int
     */
    private $deliveryCost;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $adminId;

    /**
     * @param string $aggregateId
     * @param string $supplierId
     * @param array $hashes
     * @param int $pricePerPackage
     * @param int $deliveryCost
     * @param string $currency
     * @param string $adminId
     */
    public function __construct(
        string $aggregateId,
        string $supplierId,
        array $hashes,
        int $pricePerPackage,
        int $deliveryCost,
        string $currency,
        string $adminId
    ) {
        $this->aggregateId = $aggregateId;
        $this->supplierId = $supplierId;
        $this->pricePerPackage = $pricePerPackage;
        $this->deliveryCost = $deliveryCost;
        $this->hashes = $hashes;
        $this->adminId = $adminId;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return 'OrderOpenedEvent';
    }

    /**
     * @return ParametersBag
     */
    public function getParametersBag() : ParametersBag
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);
        $parameters->setParameter('supplierId', $this->supplierId);
        $parameters->setParameter('pricePerPackage', $this->pricePerPackage);
        $parameters->setParameter('deliveryCost', $this->deliveryCost);
        $parameters->setParameter('currency', $this->currency);
        $parameters->setParameter('hashes', $this->hashes);
        $parameters->setParameter('adminId', $this->adminId);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return OrderOpenedEvent
     */
    public static function fromParameters(array $parameters) : OrderOpenedEvent
    {
        return new self(
            $parameters['aggregateId'],
            $parameters['supplierId'],
            $parameters['hashes'],
            $parameters['pricePerPackage'],
            $parameters['deliveryCost'],
            $parameters['currency'],
            $parameters['adminId']
        );
    }
}
