<?php
/**
 * Created by PhpStorm.
 * User: mszoltysek
 * Date: 28.06.2016
 * Time: 14:32
 */

namespace Domain\CoJemy\Order;


class UserId
{
    /** @var string */
    private $id;

    /**
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return UserId
     */
    public static function generate() : UserId
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return UserId
     */
    public static function fromString(string $id) : UserId
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->id;
    }
}
