<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Order\Positions\PositionId;
use Domain\SharedKernel\Date;
use Domain\SharedKernel\Money;

class Position
{
    /** @var  UserId */
    private $userId;

    /** @var  Dish */
    private $dish;

    /** @var  string */
    private $userNick;

    /** @var  PositionId */
    private $id;

    /** @var  Date */
    private $creationDate;

    /**
     * Position constructor.
     * @param string $userId
     * @param string $dishId
     * @param string $dishName
     * @param int $priceAmount
     * @param string $priceCurrency
     * @param string $userNick
     * @param string|null $id
     * @param string|null $creationDate
     */
    public function __construct(
        string $userId,
        string $dishId,
        string $dishName,
        int $priceAmount,
        string $priceCurrency,
        string $userNick,
        string $id = null,
        string $creationDate = null
    ) {
        $price = Money::fromString($priceAmount, $priceCurrency);

        $this->userId = UserId::fromString($userId);
        $this->dish = $dishId ? Dish::withId($dishName, $dishId, $price) : Dish::withoutId($dishName, $price);
        $this->userNick = $userNick;
        $this->id = $id ? PositionId::fromString($id) : PositionId::generate();
        $this->creationDate = new Date($creationDate ? $creationDate : 'now');
    }

    /**
     * @return UserId
     */
    public function getUserId() : UserId
    {
        return $this->userId;
    }

    /**
     * @return Dish
     */
    public function getDish() : Dish
    {
        return $this->dish;
    }

    /**
     * @return string
     */
    public function getUserNick() : string
    {
        return $this->userNick;
    }

    /**
     * @return PositionId
     */
    public function getId() : PositionId
    {
        return $this->id;
    }

    /**
     * @return Date
     */
    public function getCreationDate() : Date
    {
        return $this->creationDate;
    }
}
