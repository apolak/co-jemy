<?php

namespace Domain\CoJemy\Order\Payments;

use Domain\CoJemy\Order\Positions\NullPositionId;
use Domain\CoJemy\Order\Positions\PositionId;
use Domain\CoJemy\Order\Positions\UniqIdPositionId;
use Domain\SharedKernel\Money;

class Payment
{
    /** @var PaymentId*/
    private $id;

    /** @var string */
    private $payerName;

    /** @var  PositionId */
    private $positionId;

    /** @var Money */
    private $paymentValue;

    /**
     * @param string $id
     * @param string $payerName
     * @param Money $paymentValue
     * @param string|null $positionId
     */
    private function __construct(
        string $id,
        string $payerName,
        Money $paymentValue,
        PositionId $positionId
    ) {
        $this->id = PaymentId::fromString($id);
        $this->payerName = $payerName;
        $this->paymentValue = $paymentValue;
        $this->positionId = $positionId;
    }

    /**
     * @param string $id
     * @param string $payerName
     * @param int $paymentAmount
     * @param string $paymentCurrency
     * @param string $positionId
     * @return Payment
     */
    public static function withPositionId(
        string $id,
        string $payerName,
        int $paymentAmount,
        string $paymentCurrency,
        string $positionId
    ) {
        return new self(
            $id,
            $payerName,
            new Money($paymentAmount, new Money\Currency($paymentCurrency)),
            UniqIdPositionId::fromString($positionId)
        );
    }

    /**
     * @param string $id
     * @param string $payerName
     * @param int $paymentAmount
     * @param string $paymentCurrency
     * @return Payment
     */
    public static function withoutPositionId(
        string $id,
        string $payerName,
        int $paymentAmount,
        string $paymentCurrency
    ) {
        return new self(
            $id,
            $payerName,
            new Money($paymentAmount, new Money\Currency($paymentCurrency)),
            NullPositionId::generate()
        );
    }

    /**
     * @return PaymentId
     */
    public function getId() : PaymentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPayerName() : string
    {
        return $this->payerName;
    }

    /**
     * @return Money
     */
    public function getPaymentValue() : Money
    {
        return $this->paymentValue;
    }

    /**
     * @return PositionId
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    public function isEqualTo(Payment $payment)
    {
        return (string) $this->id === (string) $payment->id;
    }
}
