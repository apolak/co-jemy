<?php

namespace Domain\CoJemy\Order\Positions;

interface PositionId
{
    /**
     * @return PositionId
     */
    public static function generate() : PositionId;

    /**
     * @param string $id
     * @return PositionId
     */
    public static function fromString(string $id) : PositionId;

    /**
     * @param PositionId $positionId
     * @return bool
     */
    public function isEqualTo(PositionId $positionId);

    /**
     * @return string|null
     */
    public function getValue();
}
