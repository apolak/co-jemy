<?php

namespace Domain\CoJemy\Order\Positions;

class UniqIdPositionId implements PositionId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return PositionId
     */
    public static function generate() : PositionId
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return PositionId
     */
    public static function fromString(string $id) : PositionId
    {
        return new self($id);
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return $this->id;
    }

    /**
     * @param PositionId $positionId
     * @return bool
     */
    public function isEqualTo(PositionId $positionId)
    {
        return $this->id === $positionId->getValue();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}
