<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Exception\Order\ParametersBag\UnknownParameterNameException;

class ParametersBag
{
    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @param string $parameterName
     * @return bool
     */
    public function hasParameter(string $parameterName) : bool
    {
        return isset($this->parameters[$parameterName]);
    }

    /**
     * @param string $parameterName
     * @param mixed $parameterValue
     */
    public function setParameter(string $parameterName, $parameterValue)
    {
        $this->parameters[$parameterName] = $parameterValue;
    }

    /**
     * @param string $parameterName
     * @return mixed
     * @throws UnknownParameterNameException
     */
    public function getParameter(string $parameterName)
    {
        if (!$this->hasParameter($parameterName)) {
            throw new UnknownParameterNameException(sprintf("Parameter %s is unknown.", $parameterName));
        }
        
        return $this->parameters[$parameterName];
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        return $this->parameters;
    }
}
