<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Type;
use Domain\SharedKernel\Money;

class Dish
{
    /** @var  string */
    private $name;

    /** @var  int */
    private $id;

    /** @var  Price */
    private $price;

    /**
     * @param string $name
     * @param string  $id
     * @param Money $price
     */
    private function __construct(string $name, string $id, Money $price)
    {
        $this->name = $name;
        $this->id = $id;
        $this->price = new Price(Type::dishCost(), $price);
    }

    /**
     * @param string $name
     * @param string $id
     * @param Money $price
     * @return Dish
     */
    public static function withId(string $name, string $id, Money $price) : Dish
    {
        return new Dish($name, $id, $price);
    }

    /**
     * @param string $name
     * @param Money $price
     * @return Dish
     */
    public static function withoutId(string $name, Money $price) : Dish
    {
        return new Dish($name, null, $price);
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return Price
     */
    public function getPrice() : Price
    {
        return $this->price;
    }
}
