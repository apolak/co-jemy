<?php

namespace Domain\CoJemy\Order\Prices;

use Domain\SharedKernel\Money;

class Price
{
    /**
     * @var Type
     */
    private $type;

    /**
     * @var Money
     */
    private $amount;

    public function __construct(Type $type, Money $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
    }

    /**
     * @return Type
     */
    public function getType() : Type
    {
        return $this->type;
    }

    /**
     * @return Money
     */
    public function getAmount() : Money
    {
        return $this->amount;
    }
}
