<?php

namespace Domain\CoJemy;

interface SupplierRepository
{
    /**
     * @param string $supplierId
     * @return Supplier|null
     */
    function findSupplier(string $supplierId);
}
