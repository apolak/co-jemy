<?php

namespace Domain\SharedKernel\Money;

use Domain\SharedKernel\Money\Exception\UnknownCurrencyException;

class Currency
{
    private static $currencies = [
        'PLN' => [
            'default_fraction_digits' => 2,
            'sub_unit' => 100
        ],
        'EUR' => [
            'default_fraction_digits' => 2,
            'sub_unit' => 100
        ]
    ];

    /**
     * @var string
     */
    private $code;

    /**
     * @var int
     */
    private $subUnit;

    /**
     * @var int
     */
    private $fractionDigits;

    public function __construct(string $code)
    {
        $code = strtoupper($code);

        if (!array_key_exists($code, self::$currencies)) {
            throw new UnknownCurrencyException(sprintf('"%s" is unknown currency', $code));
        }

        $this->code = $code;
        $this->subUnit = self::$currencies[$code]['sub_unit'];
        $this->fractionDigits = self::$currencies[$code]['default_fraction_digits'];
    }

    public function getSubUnit() : int
    {
        return $this->subUnit;
    }

    public function getFractionDigits() : int
    {
        return $this->fractionDigits;
    }

    public function isEqualTo(self $other) : bool
    {
        return $this->code === $other->code;
    }

    public function __toString() : string
    {
        return $this->code;
    }
}
