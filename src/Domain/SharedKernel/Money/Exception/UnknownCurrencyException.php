<?php

namespace Domain\SharedKernel\Money\Exception;

class UnknownCurrencyException extends \Exception
{
}
