<?php

namespace Tests\Repository;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Bundle\CoJemyCore\CoreBundle\Repository\SupplierRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class DatabaseSupplierRepository extends SupplierRepository
{
    const ENTITY_PATH = 'CoreBundle:FoodSupplier';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($this->getSupplierRepository());
    }

    /** @inheritdoc */
    public function addSupplier($id, $deliveryPrice, $pricePerPackage, $freeDeliveryThreshold = 100)
    {
        $supplier = new FoodSupplier();
        $supplier
            ->setId($id)
            ->setDeliveryCost($deliveryPrice)
            ->setSinglePackageCost($pricePerPackage)
            ->setFreeDeliveryThreshold($freeDeliveryThreshold)
            ->setName('test_supplier')
            ->setPhoneNumber('fake_number')
            ->setWebsiteUrl('some_website');

        $this->entityManager->persist($supplier);
        $this->entityManager->flush($supplier);
    }

    public function clear()
    {
        $this->entityManager->createQuery('DELETE ' . self::ENTITY_PATH)->execute();

        $metaData = $this->entityManager->getClassMetadata(self::ENTITY_PATH);
        $this->entityManager->getConnection()->exec('ALTER SEQUENCE ' . $metaData->getTableName() . '_id_seq RESTART WITH 1');

        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    /**
     * @return EntityRepository
     */
    private function getSupplierRepository() : EntityRepository
    {
        return $this->entityManager->getRepository(FoodSupplier::class);
    }
}
